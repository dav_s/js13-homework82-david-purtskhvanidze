import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { addChar } from './keypad.actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  password!: Observable<string>

  constructor(private store: Store<{ password: string }>) {
    this.password = store.select('password');
  }


  keyClick(char: string) {
    this.store.dispatch(addChar({char: char}));
  }
}
